# DiffuserAnalysis

Usage of the plotting scripts is documented here: https://www.overleaf.com/read/jnmffrpjntjj. 
In particular, section 2.3 describes diffuser profile plotting (scripts plot_1D.py, plot_2D.py, and plot_waveform.py), and section 3.3. describes plotting of fibre dispersion and attenuation (script plot_fibres.py).
