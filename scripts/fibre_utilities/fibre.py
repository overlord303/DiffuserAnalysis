import pandas as pd

from utilities.readscan import *
from utilities.waveform import Waveform

class Multiton(type):
    
    def __init__(cls, name, bases, class_dict):
        cls._registry = dict()

    def __call__(cls, uid, **kwargs):
        if uid not in cls.fibre_uids():
            cls._registry[uid] = cls.__new__(cls)
            cls._registry[uid].__init__(uid, **kwargs)
            return cls._registry[uid]
        else:
            fibre = cls._registry[uid]
            for attr, value in kwargs.items():
                if not hasattr(fibre, attr):
                    raise RuntimeError('Invalid keyword argument {} to Fibre() constructor'.format(attr))
                if getattr(fibre, attr) is None and value is not None:
                    setattr(fibre, attr, value)
                if getattr(fibre, attr) is not value and value is not None:
                    raise RuntimeError('Fibre attributes do not match existing object with uid {}'.format(uid))
            return fibre


class Fibre(object):
    __metaclass__ = Multiton

    @classmethod
    def get_fibre(cls, uid):
        # Returns None if no fibre found
        return cls._registry.get(uid)

    @classmethod
    def fibres_items(cls):
        '''
            To loop through fibres:
            for uid, fibre in Fibre.fibres_items():
                ...
        '''
        return cls._registry.items()
    
    @classmethod
    def fibres(cls):
        '''
            To loop through fibres:
            for fibre in Fibre.fibres():
                ...
        '''
        return cls._registry.values()
    
    @classmethod
    def fibre_uids(cls):
        '''
            To loop through fibres:
            for uid in Fibre.fibre_uids():
                ...
        '''
        return cls._registry.keys()
   
    @classmethod
    def add_fibre_data(cls, filename):
        if filename[-4:] not in ('.csv', 'root'):
            raise RuntimeError('Unknown filetype for file {}, please pass attenuation results as .csv files or dispersion results as .root files'.format(filename))
        fibre = None
        for uid in cls.fibre_uids():
            if uid in filename:
                fibre = cls(uid)
                break
        if fibre is None:
            raise RuntimeError('Filename {} does not contain unique identifier in registry'.format(filename))

        # determine wheter attenuation or dispersion filename was passed and set value
        if '.csv' in filename:
            if fibre.att is None:
                fibre.set_attenuation_from_file(filename)
            else:
                raise RuntimeError('Fibre attenuation already set for fibre with this unique identifier.')
        elif '.root' in filename:
            if fibre.disp is None:
                fibre.set_dispersion_from_file(filename)
            else:
                raise RuntimeError('Fibre dispersion already set for fibre with this unique identifier.')
        return fibre

    def __init__(self, uid, fibtype='', length=0, na='', disp=None, att=None):
        self._uid = uid
        self.fibtype = fibtype
        self.length = length
        self.na = na
        self.disp = disp
        self.att = att

    @classmethod
    def populate_from_config(cls, fibre_conf='fibre_conf.txt'):
        with open(fibre_conf, 'r') as datasheet:
            for line in datasheet.readlines():
                if line.strip()[0] != '#':
                    uid, fibtype, length, na = line.strip().split(',')
                    cls(uid, fibtype=fibtype, length=float(length), na=na)

    @property
    def uid(self):
        return self._uid

    def set_attenuation_from_file(self, filename):
        df = pd.read_csv(filename, skiprows=14)
        df.columns = ['Sample', 'Date', 'Time', 'Power [W]', 'DateTime']
        self.att = 1000000*df['Power [W]'].mean() # attenuation in uW

    def set_dispersion_from_file(self, filename):
        data = [readscan(filename, "diffuser")]
        disp = 0
        for d in data:
            avg_PD = 0
            for scanpoint in d.scanpoints:
                waveform_PD = scanpoint.getWaveformPD(None, False, False)
                avg_PD += waveform_PD.fwhm
            disp += avg_PD / len(d.scanpoints)
        self.disp = disp / len(data)

    @classmethod
    def count_matches(cls, attr, value):
        return sum(getattr(fibre, attr) == value for fibre in cls._registry.values())

    @classmethod
    def count_matches_a_for_unequal_b(cls, attr_a, value_a, attr_b, value_b):
        return sum(((getattr(fibre, attr_a) == value_a) and (getattr(fibre, attr_b) != value_b)) for fibre in cls._registry.values())
