import math
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from operator import itemgetter

from fibre import Fibre

 
colors = ['tab:blue', 'tab:red', 'tab:orange', 'tab:green', 'tab:cyan', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive']
patterns = ['', '///', 'xxx', '|||', '---', '+++',  'ooo', 'OOO', '...', '***']
width = 0.4 # of bar
separation = 0.2 # between different fibres of same na


# compare  values of attribute attr (disp or att) of fibres 
# for all lengths passed in plot_lengths

def x_axis_slots(entries, unique_nas):
    # calculate the distances between bars on x-axis
    na_groups = [] # nr of elements per group, # groups = len of this list
    for unique_na in unique_nas:
        na_groups.append(sum(unique_na == entry[0] for entry in entries))

    x_coords_uniq = [] # want sth like: 0, 0.6, 0.9, 1.2, 1.5, 2.1, 2.55, 3
    x_labels_uniq = []

    sum_fibs_prev = 0
    offset = 0
    for na_group, n_fibs in enumerate(na_groups):
        for i in range(0, n_fibs):
           x_coords_uniq.append(na_group*(1+separation)/2 - i*width*(na_group-1) + offset*width)
        if n_fibs > 1:
            offset += n_fibs-1
        
    x_labels = [na[1] for na in entries]
    return x_coords_uniq, x_labels


def bar_style(fibre, uniq_nas, plot_lengths):
    # determine color based on na group index -> same index in colors and
    # determine pattern based on fibre length's index in plot_lengths
    col = colors[uniq_nas.index(fibre.na)]
    pat = patterns[plot_lengths.index(fibre.length)]
    return col, pat

def patch_style(patch1, patch2, patch3, col, pat, alpha):
        patch3.set_edgecolor('k')
        patch3.set_facecolor('none')
        patch2.set_facecolor(col)
        patch2.set_alpha(alpha)
        patch1.set_facecolor('none')
        patch1.set_edgecolor(col)
        patch1.set_hatch(pat)

def bar_position(fibre, x_slots_uniq, x_labels_uniq):
    return x_slots_uniq[x_labels_uniq.index(fibre.fibtype)]  


def set_titles(attr,plot_difference, plot_lengths, convert_units):
    y_label = ''
    if not plot_difference:
        title = 'Absolute Values of '
        if attr == 'att':
            title+= 'Attenuation (Power)'
            y_label = 'Power [uW]'
        if attr == 'disp':
            title+= 'Dispersion (FWHM)'
            y_label = 'fwhm of PD wf [ns]'
    else:
        title = ''
        if attr == 'att':
            if convert_units:
                title+='Relative Power Loss'
                #y_label = '-10*log10(Power({} m)/Power({} m))*(1000 m/{} m) [dB/km]'.format(plot_lengths[1],plot_lengths[0],plot_lengths[0]-plot_lengths[1])
                y_label = 'Power Loss [dB/km]'
            else:
                title+= 'Absolute Power Loss'
                #y_label = 'Power({})-Power({}) [uW]'.format(plot_lengths[0],plot_lengths[1])
                y_label = 'Power Loss [uW]'
        if attr == 'disp':
            title+= 'FWHM change'
            y_label = 'Difference in fwhm [ns]'
        title += ' over {} m'.format(plot_lengths[1]-plot_lengths[0])
    return title, y_label


def plot(plot_lengths, attr='att', outdir=None, plot_format='pdf',plot_difference=False, convert_units=False):
    # TODO
    # sanity checks: if plot_difference, only 2 plot_lengths passed
    #                if convert_units, attr == 'att'
    # if plot absolute values
    title, y_label = set_titles(attr, plot_difference, plot_lengths, convert_units)
    alpha = 0.9 if plot_difference else 0.4

    # determine how many other fibres have the same numerical aperture and group by na
    entries = sorted(sorted(set([tuple([fibre.na, fibre.fibtype]) for fibre in Fibre.fibres() if fibre.length in plot_lengths]), key=itemgetter(1)), key=itemgetter(0))
    nas_uniq = sorted(set([fibre.na for fibre in Fibre.fibres() if fibre.length in plot_lengths]))
    x_slots_uniq, x_labels_uniq = x_axis_slots(entries, nas_uniq)
    plot_x = []
    plot_values = []
    plot_cols = []
    plot_pats = []
    
    # for each entry, get data for all lenghts and append value and x coord to corr lists
    for fibre in Fibre.fibres():
        if not plot_difference and fibre.length not in plot_lengths:
            continue
        if plot_difference and fibre.length != plot_lengths[0]: # want: long-short
            continue
        if plot_difference and fibre.length == plot_lengths[0]: # check if corr long fib exists
            partner_exists = False
            for f in Fibre.fibres():
                if f.length == plot_lengths[1] and f.fibtype == fibre.fibtype:
                    partner_exists = True
            if not partner_exists:
                continue

        x = bar_position(fibre, x_slots_uniq, x_labels_uniq)

        val = getattr(fibre, attr)  
        if plot_difference:
            long_fibre_val = getattr(next(f for f in Fibre.fibres() if (f.length==plot_lengths[1] and f.fibtype==fibre.fibtype)), attr)
            if convert_units:
                val = (-10)*math.log10((val/long_fibre_val))*(1000/plot_lengths[1]-plot_lengths[0])
            else:
                val = abs(val - long_fibre_val)

        plot_x.append(x)
        plot_values.append(val)

        col, pat = bar_style(fibre, nas_uniq, plot_lengths)
        plot_cols.append(col)
        plot_pats.append(pat)

    fig, ax = plt.subplots(1,1)
    bar1 = ax.bar(plot_x, plot_values, width-0.05)
    bar2 = ax.bar(plot_x, plot_values, width-0.05)
    bar3 = ax.bar(plot_x, plot_values, width-0.05)
    ax.set_xticks(x_slots_uniq)
    ax.set_xticklabels(x_labels_uniq, rotation=30,horizontalalignment='right')
    ax.set_ylabel(y_label)
    ax.set_title(title)
    
    for i, (patch1, patch2, patch3, col, pat) in enumerate(zip(bar1.patches, bar2.patches, bar3.patches, plot_cols, plot_pats)):
        patch_style(patch1, patch2, patch3, col, pat, alpha)

    legend_elements = []
    for i, na in enumerate(nas_uniq):
        legend_elements.append(Patch(facecolor=colors[i], edgecolor='k', label=na))
    if len(nas_uniq) < len(plot_lengths):
        for i in range(0, len(plot_lengths) - len(nas_uniq)):
            legend_elements.append(Patch(edgecolor='none', facecolor='none', label=''))

    if not plot_difference:
        for i, length in enumerate(plot_lengths):
            l = str(length) + ' m'
            if i == 0:
                legend_elements.append(Patch(edgecolor=(0,0,0,1), facecolor=(0,0,0,alpha), label=l))
            else:
                legend_elements.append(Patch(facecolor='none', edgecolor='k', label=l, hatch=patterns[i]))

        if len(nas_uniq) > len(plot_lengths):
            for i in range(0, len(nas_uniq) - len(plot_lengths)):
                legend_elements.append(Patch(edgecolor='none', facecolor='none', label=''))

    legend_title = 'Num. Aperture' if plot_difference else 'Num. Aperture         Fibre Length'
    if attr == 'att':
        fig.legend(title=legend_title, handles=legend_elements, ncol = 2, bbox_to_anchor=(0,0,1,1), bbox_transform=ax.transAxes, loc='upper left')
    elif attr == 'disp':
        fig.legend(title=legend_title, handles=legend_elements, ncol = 2, bbox_to_anchor=(0,0,1,1), bbox_transform=ax.transAxes, loc='lower left')
    else:
        fig.legend(title=legend_title, handles=legend_elements, ncol = 2, bbox_to_anchor=(0,0,1,1), bbox_transform=ax.transAxes)

    plt.tight_layout()
    plt.show()

    if outdir is not None:
        save_path = outdir[0] + attr 
        if plot_difference:
            save_path += '_diff{}m'.format(plot_lengths[1]-plot_lengths[0]) 
        if convert_units:
            save_path += '_dB_per_km' 
        save_path+= '.'+plot_format
        fig.savefig(save_path)
        print('Saving output plots at {}'.format(save_path))
