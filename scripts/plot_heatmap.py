from argparse import ArgumentParser
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import numpy as np
import itertools
import math
import re
from utilities.readscan import readscan
from utilities.waveform import Waveform, DEFAULT_FMAX

class XVarOptions:
    ANGLE = "phi"
    COSANGLE = "cos phi"
    ALL_CHOICES = [ANGLE, COSANGLE]

class YVarOptions:
    HEIGHT = "height"
    VERTANGLE = "vertangle"
    ANGLE = "theta"
    COSANGLE = "cos theta"
    ALL_CHOICES = [HEIGHT, VERTANGLE, ANGLE, COSANGLE]  

def plot(data, bins, labels, output=None, norm=None, xlim=None, ylim=None, zlim=None, centre_height=None, pmt_dist=None, xvar=XVarOptions.ANGLE, yvar=YVarOptions.HEIGHT, zvar="area", fmax=DEFAULT_FMAX, dohemisphere=False):
    for i, (d, l) in enumerate(zip(data, labels)):
        fig = plt.figure(figsize=(12,5))
        ax1 = fig.add_subplot(1, 2, 1)
        ax2 = fig.add_subplot(1, 2, 2)
        _plot_series(fig, ax1, ax2, d.scanpoints, bins, l, norm, xlim=xlim, ylim=ylim, zlim=zlim, centre_height=centre_height, pmt_dist=pmt_dist, xvar=xvar, yvar=yvar, zvar=zvar, fmax=fmax, dohemisphere=dohemisphere)
        if xlim:
            for ax in [ax1, ax2]:
                ax.set_xlim(xlim)
        if ylim:
            for ax in [ax1, ax2]:
                ax.set_ylim(ylim)
        fig.tight_layout()
        if output is not None:
            _writefig(fig, output)
    
    plt.show()
    return

def _plot_series(fig, ax1, ax2, data, bins, label, norm, xlim=None, ylim=None, zlim=None,centre_height=None, pmt_dist=None, xvar=XVarOptions.ANGLE, yvar=YVarOptions.HEIGHT, zvar="area", fmax=DEFAULT_FMAX, dohemisphere=False):
    if xvar == XVarOptions.COSANGLE:
        xtransform = lambda x: math.cos(x.coord_1*math.pi/180.)
    elif xvar == XVarOptions.ANGLE:
        xtransform = lambda x: x.coord_1

    if yvar == YVarOptions.HEIGHT or YVarOptions.ANGLE:
        ytransform = lambda x: x.coord_2
    elif xvar == YVarOptions.COSANGLE:
        ytransform = lambda x: math.cos(x.coord_2*math.pi/180.)
    elif yvar == YVarOptions.VERTANGLE:
        ytransform = lambda x: np.sign(x.coord_2-centre_height) * math.atan(abs(x.coord_2-centre_height) / pmt_dist) * 360 / (2*math.pi)

    if yvar == YVarOptions.VERTANGLE:
        ztransform = lambda x: getattr(Waveform(x.axis_time, x.samples_PMT, fmax), zvar) * fluxtransform(pmt_dist, x.coord_2-centre_height)
    else:
        ztransform = lambda x: getattr(Waveform(x.axis_time, x.samples_PMT, fmax), zvar)

    X_list, Y_list, Z_list = [], [], []
    for d in data:
        if rangecheck(xlim, ylim, d.coord_1, d.coord_2):
            X_list.append(xtransform(d))
            Y_list.append(ytransform(d))
            Z_list.append(ztransform(d) * (hemisphere_correction(d.coord_1, zvar) if dohemisphere else 1))

    X = np.array(X_list, dtype=float)
    Y = np.array(Y_list, dtype=float)    
    Z = np.array(Z_list, dtype=float)
    my_cmap = plt.cm.RdBu_r

    h1 = ax1.hist2d(X, Y, weights=Z, cmap=my_cmap, bins=bins)
    # normalise second histogram to 1, independent of chosen binning
    max_h1 = 0
    for row in h1[0]:
        if max(row) > max_h1:
            max_h1 = max(row)
    if norm: 
        Z_norm = Z/norm
    else:
        Z_norm = Z/max_h1
    h2 = ax2.hist2d(X, Y, weights=Z_norm, vmin=0, vmax=1, cmap=my_cmap, bins=bins)
    cbar1 = fig.colorbar(h1[3], ax=ax1)
    cbar2 = fig.colorbar(h2[3], ax=ax2)
    cbar1.ax.set_ylabel("area [V*ns]")
    cbar2.ax.set_ylabel("relative area")
    
    ax1.set_xlabel("horizontal angle [degrees]")
    ax1.set_ylabel("vertical angle [degrees]")
    ax2.set_xlabel("horizontal angle [degrees]")
    ax2.set_ylabel("vertical angle [degrees]")
    ax1.set_title(label + " - absolute")
    if not norm:
        ax2.set_title(label + " - normalised")
    else:
        titlestring = label + " - norm. to " + str(norm.round(1))
        ax2.set_title(titlestring)

def rangecheck(xlim, ylim, xcoord, ycoord):
    x_in_range = xlim is None or xlim[0] <= xcoord <= xlim[1]
    y_in_range = ylim is None or ylim[0] <= ycoord <= ylim[1]
    return x_in_range and y_in_range

# Transform incoming signal (flux) to match angle in vertical plane at constant distance pmt_dist
# This is needed in order to plot signal vs. angle in vertical plane, since experimental setup only moves PMT straight up/down
def fluxtransform(d, h):
    return math.pow(math.pow(d, 2) + math.pow(h, 2), 3/2) / math.pow(d, 3)

def hemisphere_correction(angle, var):
    if var in ["area", "amplitude"]:
        angle_corr = float(angle)*np.pi/180. # Convert to radians
        return 1.0 / (1.0 - (abs(angle_corr) / np.pi))

def find_global_max(data, xlim, ylim, fmax=DEFAULT_FMAX, yvar=YVarOptions.HEIGHT, zvar="area", dohemisphere=False):
    glob_max = 0
    if yvar == YVarOptions.VERTANGLE:
        ztransform = lambda x: getattr(Waveform(x.axis_time, x.samples_PMT, fmax), zvar) * fluxtransform(pmt_dist, x.coord_2-centre_height)
    else:
        ztransform = lambda x: getattr(Waveform(x.axis_time, x.samples_PMT, fmax), zvar)
    for file in data: 
        Z_list = []
        for d in file.scanpoints:
            if rangecheck(xlim, ylim, d.coord_1, d.coord_2):
                Z_list.append(ztransform(d) * (hemisphere_correction(d.coord_1, zvar) if dohemisphere else 1))
        if max(Z_list) > glob_max:
            glob_max = max(Z_list)
    return glob_max


def parsecml():
    parser = ArgumentParser()
    parser.add_argument("-o", "--output", help="Output filename", default=None)
    parser.add_argument("--labels", help="Comma separated list of names", default=None)
    parser.add_argument("filename", nargs="+", help="Input filename.")
    parser.add_argument("--combinefiles", action="store_true", help="Combine input files and plot them as a single series.")
    parser.add_argument("--xlim", type=str, help="Set x-axis limits.", default=None)
    parser.add_argument("--ylim", type=str, help="Set y-axis limits.", default=None)
    parser.add_argument("--zlim", type=str, help="Set z-axis limits.", default=None)
    parser.add_argument("--xvar", type=str, default=XVarOptions.ANGLE, choices=XVarOptions.ALL_CHOICES)
    parser.add_argument("--yvar", type=str, default=YVarOptions.ANGLE, choices=YVarOptions.ALL_CHOICES)
    parser.add_argument("--zvar", type=str, default="area", help="Choose z-axis variable")
    parser.add_argument("--centre_height", type=str, default="0.0", help="Set height where diffuser is centred vs. PMT [mm]. Only used when yvar=vertangle")
    parser.add_argument("--pmt_dist", type=str, help="Set PMT distance from diffuser [mm]. Only used when yvar=vertangle")
    parser.add_argument("--hemisphere-correction", "-c", action="store_true", help="Apply correction (1-angle/pi) to intensity measurments to account for hemisphere geometry.")
    parser.add_argument("--filter", action="store_true", help="Switch on low-pass filter.")
    parser.add_argument("--fmax", help="Set frequency cut for low pass filter.", default=DEFAULT_FMAX, type=float)
    parser.add_argument("--norm", help="Option to normalise plots for all different files to the same number. Default is normalising to the max value per file.", default=None, type=float)
    parser.add_argument("--normglobmax",  action="store_true", help="Option to normalise plots for all different files to the maximal area found globally in all files. Default is normalising to the max value per file. Overrides the --norm option if a number is specified there.")
    parser.add_argument("--bins", help="Option to specify the number of bins [x,y], specify two numbers as comma separated list. Default is one bin per datapoint", default="21,21")
    return parser.parse_args()

def main():
    args = parsecml()
    matplotlib.rc('legend', fontsize="x-small")
    data = [readscan(f, "diffuser") for f in args.filename]
    bins_csv = args.bins.split(",")
    bins = [int(bins_csv[0]), int(bins_csv[1])] 
    if args.combinefiles:
        data = [combinefiles(data)]
    labels = []
    for i, f in enumerate(args.filename):
        if args.labels:
            labels = args.labels.split(",")
        else:
            index = f.rfind("/")
            if index == -1:
                labels.append(f)
            else: 
                labels.append(f[index+1:-5])
    xlim, ylim, zlim, centre_height, pmt_dist = None, None, None, None, None
    if args.xlim:
        low, high = args.xlim.split(",")
        xlim = (float(low), float(high))
    if args.ylim:
        low, high = args.ylim.split(",")
        ylim = (float(low), float(high))
    if args.zlim:
        low, high = args.zlim.split(",")
        zlim = (float(low), float(high))
    if args.centre_height:
        centre_height = float(args.centre_height)
    if args.pmt_dist:
        pmt_dist = float(args.pmt_dist)
    fmax=args.fmax if args.filter else None
    
    norm = args.norm
    if args.normglobmax:
        norm = find_global_max(data, xlim, ylim, fmax, args.yvar, args.zvar, dohemisphere=args.hemisphere_correction) 

    plot(data, bins, labels, args.output, norm=norm, xlim=xlim, ylim=ylim, zlim=zlim, centre_height=centre_height, pmt_dist=pmt_dist, xvar=args.xvar, yvar=args.yvar, zvar=args.zvar, fmax=fmax, dohemisphere=args.hemisphere_correction)
    return

if __name__ == "__main__":
    main()
