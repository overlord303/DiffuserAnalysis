#usr/bin/env python2

import os
from argparse import ArgumentParser
from itertools import chain

from fibre_utilities.fibre import Fibre 
from fibre_utilities.plotting import *


verbose = False

def valid_file(infile):
    if infile[0] == '.' and infile[1] != 0:
        return False
    elif infile[-5:] == '.root' or infile[-4:] == '.csv':
        return True
    else:
        raise RuntimeError('File {}: filetype not supported. Please only pass .root or .csv files.'.format(infile))
        return False


if __name__ == "__main__":
    parser = ArgumentParser(description='Pass input directories: -d <path to dir with ROOT files from dispersion measurements -a <path to dir with csv files from attenuation measurements. It is also possible to pass single files instead.')
    parser.add_argument('-i', metavar='INPUT_DIR', type=str, nargs='+', help='Directory (one or more) containing all ROOT files from dispersion measurements AND all csv files from attenuation measurements.')
    parser.add_argument('-o', metavar='OUTPUT_DIR', type=str, nargs=1, default=None, help='Directory where the output plots should be saved- plots will not be saved if this is not specified.')
    parser.add_argument('--fibre-db', metavar='FIBRE_DB_CONF_FILE', nargs=1, type=str, default="fibre_conf.csv", help="Path to csv file with database of all fibres. Should contain one line per fibre, formatted as:  <uniqueID_in_filenames>,<Fibre Name>,<length>,<numerical_aperture>")
    args = vars(parser.parse_args())

    Fibre.populate_from_config(args['fibre_db'])

    data_files = chain((args['i'][0] + '/' + fn for fn in os.listdir(args['i'][0]) if valid_file(fn)))

    for i, inputdir in enumerate(args['i']):
        if i == 0:
            continue
        else:
            data_files = chain(
                    (data_files),
                    (inputdir + '/' + fn for fn in os.listdir(inputdir) if valid_file(fn)))

    for infile in data_files:
        fibre = Fibre.add_fibre_data(infile)
        if verbose: print('  Adding data from file {} to fibre {}'.format(infile, fibre.fibtype if fibre else fibre))

    # create plots and save if -o flag was passed with path to plot dir
    plot([1, 20, 100, 120, 200], 'disp', args['o'])
    #plot([1, 20], 'disp', args['o'], plot_difference=True)
    #plot([1, 100], 'disp', args['o'], plot_difference=True)
    #plot([1, 20, 100], 'att', args['o'])
    #plot([1, 20], 'att', args['o'], plot_difference=True)
    #plot([1, 100], 'att', args['o'], plot_difference=True)
    #plot([1, 20], 'att', args['o'], plot_difference=True, convert_units=True)
    #plot([1, 100], 'att', args['o'], plot_difference=True, convert_units=True)
